<?php
   require ('database.php');
?>

<!DOCTYPE html>
<html>
   <head>
      <title>
         Veebiprojekt
      </title>
      <meta charset="utf-8">
      <link href="atribuudid/stiil.css" rel="stylesheet" type="text/css">
      <link href=
         "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel=
         "stylesheet" type="text/css">
      <meta content="IE=edge" http-equiv="X-UA-Compatible">
      <meta content="width = device-width, initial-scale = 1" name="viewport">
   </head>
   <body style="background-image: url(images/Sun.jpg)">
      <?php if (logged()) : ?>
      <div class="container">
         <div class="page-header">
            <h1>
               Tere tulemast <?=$_SESSION['Kasutajanimi']?>!
            </h1>
         </div>
         <div class="jumbotron">
            <h2>
               Redigeerige oma profiili siin, allpool kuvatakse Teie hetkeandmed
            </h2>
            <div class="btn-group-g" style="text-align: center; margin-top: 28px">
               <a class="btn btn-success" href="friends.php" id=
                  "btn_Friends"><span class="glyphicon glyphicon-user"></span> Sinu
               sõbrad</a> <a class="btn btn-warning" href="login_success.php" id=
                  "btn_home"><span class="glyphicon glyphicon-home"></span> Pealehele</a>
               <a class="btn btn-danger" href="logout.php" id="btn_LogOut"><span class=
                  "glyphicon glyphicon-warning-sign"></span> Logi välja</a>
            </div>
         </div>
         <div class="col-sm-4"></div>
         <form action="updateUserData.php" class="form-horizontal col-sm-4" id=
            "edit_Profile" method="post" name="edit_Profile">
            <?php $data = getUserData(); ?>
            <h3>
               Sugu
            </h3>
            <input class="form-control" name="edit_Sugu" placeholder="Sugu"
               required="" type="text" value="<?php echo $data['Sugu'] ?>">
            <h3>
               Vanus
            </h3>
            <input class="form-control" name="edit_Vanus" placeholder="Vanus"
               required="" type="text" value="<?php echo $data['Vanus'] ?>">
            <h3>
               Eesnimi
            </h3>
            <input class="form-control" name="edit_Nimi" placeholder="Eesnimi"
               required="" type="text" value="<?php echo $data['Eesnimi'] ?>">
            <h3>
               Asukoht
            </h3>
            <input class="form-control" name="edit_Asukoht" placeholder="Asukoht"
               required="" type="text" value="<?php echo $data['Asukoht'] ?>">
            <button class="btn btn-success center-block" id="edit_Nupp" type=
               "submit"><span class="glyphicon glyphicon-cog"></span> Muuda
            andmeid</button>
         </form>
         <div class="col-sm-4"></div>
      </div>
      <?php else : ?><?php header("Location: logimine.php"); ?><?php endif ?>
   </body>
</html>