<?php
require("database.php");
$username     = $_POST['reg_Kasutajanimi'];
$password     = $_POST['reg_Parool'];
$passwordConf = $_POST['reg_PassConf'];
$success      = register($username, $password, $passwordConf);
if ($success != true && $success != "done") {
    header("Location: registreerumine.php?alert=" . $success);
} elseif ($success == "done") {
    header("Location: registreerumine.php?success=" . $success);
} else {
    header("Location: registreerumine.php?alert=" . $success);
}
?>