<?php
require("database.php");
if (logged()) {
    updateUserData();
    header("Location: edit.php");
}
?>