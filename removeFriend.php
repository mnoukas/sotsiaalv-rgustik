<?php
require("database.php");
if (logged()) {
    removeFriend($_GET['id']);
    header("Location: friends.php");
}
?>