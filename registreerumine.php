<?php
	require("database.php");
?>

<!DOCTYPE html>
<html>
   <head>
      <title>Registreerumine</title>
      <meta charset="utf-8">
      <link rel="stylesheet" type="text/css" href="atribuudid/stiil.css">
      <link rel="stylesheet" type="text/css"
         href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
      <script src="https://use.fontawesome.com/210ea4385d.js"></script>
      <script src="https://code.jquery.com/jquery-2.2.3.js"
         integrity="sha256-laXWtGydpwqJ8JA+X9x2miwmaiKhn8tVmOVEigRNtP4="
         crossorigin="anonymous"></script>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width = device-width, initial-scale = 1">
   </head>
   <body style="background-image: url(images/mac.jpeg)">
      <div class="container">
      <div class="page-header">
         <h1>Registreeri end kasutajaks!</h1>
      </div>
      <div class="jumbotron">
         <p>Kõigepealt tehke endale kasutaja, detailsemaid andmeid saate muuta
            pärast kasutaja tegemist. Kasutajanime miinimumpikkuseks on 2 tähemärki ning parooli miinimumpikkuseks on 6 tähemärki!
         </p>
         <a class="btn btn-primary" id="reg_Tagasi" href="logimine.php">Tagasi
         logimislehele</a>
      </div>
      <div class="col-sm-4"></div>
      <form class="form-horizontal col-sm-4" id="reg_Form"
         action="register.php" method="POST">
         <h2>Pange pilkupüüdev kasutajanimi, nii jääte rohkem silma!</h2>
         <input type="text" class="form-control" placeholder="Username"
            name="reg_Kasutajanimi" required> <input type="password"
            class="form-control" placeholder="Password" id="reg_Password"
            name="reg_Parool" required> <input type="password"
            class="form-control" placeholder="Korda parooli"
            id="reg_PasswordConf" name="reg_PassConf" required>
         <button type="submit" class="btn btn-success center-block"
            id="reg_Nupp">Registreeri</button>
         <div id="alert" class="alert alert-danger"
            <?= getAlert($_GET) == false? 'hidden' : ""?>><?= getAlert($_GET)?></div>
         <div id="alert" class="alert alert-success"
            <?= getSuccess($_GET) == false? 'hidden' : ""?>><?= getSuccess($_GET)?></div>
      </form>
      <div class="col-sm-4"></div>
   </body>
</html>