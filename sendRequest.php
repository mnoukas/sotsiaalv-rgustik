<?php
require("database.php");
if (logged()) {
    sendRequest();
    header("Location: login_success.php");
}
?>