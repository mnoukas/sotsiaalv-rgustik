<?php 
	require("database.php"); 
?>

<!DOCTYPE html>
<html>
   <head>
      <title>Veebiprojekt</title>
      <meta content="IE=edge" http-equiv="X-UA-Compatible">
      <meta content="width = device-width, initial-scale = 1" name="viewport">
      <meta charset="utf-8">
      <link href="atribuudid/stiil.css" rel="stylesheet" type="text/css">
      <link
         href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
         rel="stylesheet" type="text/css">
      <script
         src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
      <script
         src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
   </head>
   <body style="background-image: url(images/mac.jpeg)">
      <div class="container">
         <div class="page-header">
            <h1>Anonüümne sotsiaalvõrgustik</h1>
         </div>
         <div class="jumbotron">
            <p>Selleks, et uusi tutvusi leida, peate sisse logima või looma uue
               kasutaja. Hetkel asute sisselogimislehel.
            </p>
            <div class="btn-group-g" style="text-align: center">
               <a class="btn btn-danger" href="logimine.php" id="valitud-nupp">Sisselogimine</a>
               <a class="btn btn-danger" href="registreerumine.php" id="rega-nupp">Registreerimine</a>
               <a class="btn btn-danger" href="http://enos.itcollege.ee/~mnoukas"
                  id="pealehe-nupp"> Tagasi kodulehele </a>
            </div>
         </div>
         <div class="col-sm-4"></div>
         <form class="form-horizontal col-sm-4" action="login.php"
            method="POST">
            <h2 id="loginmsg">Palun logige sisse.</h2>
            <input type="text" id="log_Kasutajanimi" name="Kasutajanimi"
               class="form-control" placeholder="Kasutajanimi" required> <input
               type="password" id="log_Parool" name="Parool" class="form-control"
               placeholder="Password" required>
            <button type="submit" id="log_nupp"
               class="btn btn-primary center-block">Logi sisse</button>
            <div id="alert" class="alert alert-danger"
               <?= getAlert($_GET) == false? 'hidden' : ""?>><?= getAlert($_GET)?></div>
         </form>
      </div>
      <div class="col-sm-4"></div>
      <form class="form-horizontal col-sm-4" id="msgBox">
         <h3>Probleemide/küsimuste korral kontakteeruge adminiga! Soovi korral
            saate jääda ka anonüümseks!
         </h3>
         <input type="text" id="msgEmail" class="form-control"
            placeholder="Emaili aadress" required>
         <textarea type="text" id="teade" class="form-control" rows="4"
            placeholder="Teie teade" required></textarea>
         <button type="button" id="clicker"
            class="btn btn-success center-block" data-toggle="modal"
            data-target="#myModal">Saada Teade!</button>
         <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                     <h4 class="modal-title">Teade!</h4>
                  </div>
                  <div class="modal-body">
                     <p>Sõnum edukalt saadetud!</p>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-default" data-dismiss="modal">
                     Sulge</button>
                  </div>
               </div>
            </div>
         </div>
      </form>
      <!-- Funktsioon, mis kontrollib, kas teatekasti on midagi kirjutatud, kui on, siis lase nuppu vajutada -->
      <script>
         $(document).ready(function(){
           $('#clicker').prop('disabled',true);
           $('#teade').keyup(function(){
             $('#clicker').prop('disabled', this.value == "" ? true : false);
           })
         });
      </script>
   </body>
</html>