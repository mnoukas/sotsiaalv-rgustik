<?php
   require ("database.php");
?>

<!DOCTYPE HTML>
<html>
   <head>
      <title>Veebiprojekt</title>
      <meta charset="utf-8">
      <link rel="stylesheet" type="text/css" href="atribuudid/stiil.css">
      <link rel="stylesheet" type="text/css"
         href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width = device-width, initial-scale = 1">
   </head>
   <body style="background-image: url(images/Sun.jpg)">
      <?php if (logged()) : ?>
      <div class="container">
         <div class="page-header">
            <h1>Tere tulemast <?=$_SESSION['Kasutajanimi']?>!</h1>
         </div>
         <div class="jumbotron">
            <h2>Olete pealehel, siin saate uusi tutvusi leida!</h2>
            <div class="btn-group-g" style="text-align: center; margin-top: 28px">
               <a class="btn btn-success" id="btn_Friends" href="friends.php"><span
                  class="glyphicon glyphicon-user"></span> Sinu sõbrad </a> <a
                  class="btn btn-info" id="btn_Edit" href="edit.php"><span
                  class="glyphicon glyphicon-pencil"></span> Muuda profiili </a> <a
                  class="btn btn-danger" id="btn_LogOut" href="logout.php"><span
                  class="glyphicon glyphicon-warning-sign"></span> Logi välja </a>
            </div>
         </div>
         <div style="overflow: auto">
            <table
               class="table table-responsive table-bordered table-striped table-hover">
               <?php $data = findPeople()?>
               <h2 id="pealkiri">Kasutajad</h2>
               <thead>
                  <tr class="active">
                     <th class="text-center">Id</th>
                     <th class="text-center">Kasutajanimi</th>
                     <th class="text-center">Sugu</th>
                     <th class="text-center">Vanus</th>
                     <th class="text-center">Eesnimi</th>
                     <th class="text-center">Asukoht</th>
                     <th class="text-center">Taotlus</th>
                  </tr>
               </thead>
               <tbody>
                  <form class="form-horizontal" action="sendRequest.php"
                     method="POST">
                     <?php foreach($data as $person) : ?>
                     <tr class="active">
                        <td> <?php echo $person['Id']?> </td>
                        <td> <?php echo $person['Kasutajanimi']?> </td>
                        <td> <?php echo $person['Sugu']?> </td>
                        <td> <?php echo $person['Vanus']?> </td>
                        <td> <?php echo $person['Eesnimi']?> </td>
                        <td> <?php echo $person['Asukoht']?> </td>
                        <td><button type="submit" name="accept" class="btn btn-primary"
                           value=<?php echo $person['Id']?>>
                           <span class="glyphicon glyphicon-star"></span> Saada
                           sõbrataotlus
                           </button>
                        </td>
                     </tr>
                     <?php endforeach ?>
                  </form>
            </table>
         </div>
      </div>
      <?php else : ?>
      <?php header("Location: logimine.php"); ?>
      <?php endif ?>
   </body>
</html>