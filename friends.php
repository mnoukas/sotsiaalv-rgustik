<?php
   require ("database.php");
?>

<!DOCTYPE HTML>
<html>
   <head>
      <title>Näoraamat</title>
      <meta charset="utf-8">
      <link rel="stylesheet" type="text/css" href="atribuudid/stiil.css">
      <link rel="stylesheet" type="text/css"
         href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width = device-width, initial-scale = 1">
   </head>
   <body style="background-image: url(images/Sun.jpg)">
      <?php if (logged()) : ?>
      <div class="container">
         <div class="page-header">
            <h1>Tere tulemast <?=$_SESSION['Kasutajanimi']?>!</h1>
         </div>
         <div class="jumbotron">
            <h2>Sinu sõbrad ning sõbrakutsed</h2>
            <div class="btn-group-g" style="text-align: center; margin-top: 28px">
               <a class="btn btn-warning" id="btn_Pealeht" href="login_success.php"><span
                  class="glyphicon glyphicon-home"></span> Pealehele </a> <a
                  class="btn btn-info" id="btn_Edit" href="edit.php"><span
                  class="glyphicon glyphicon-pencil"></span> Muuda profiili </a> <a
                  class="btn btn-danger" id="btn_LogOut" href="logout.php"><span
                  class="glyphicon glyphicon-warning-sign"></span> Logi välja </a>
            </div>
         </div>
         <div class="col-sm-6" style="overflow: auto">
            <h2>Sõbrad</h2>
            <table class="table table-bordered table-hover">
               <?php $data = showFriends()?>
               <thead>
                  <tr class="active">
                     <th class="text-center">Sõber</th>
                     <th class="text-center">Sugu, vanus ja asukoht</th>
                     <th class="text-center">Tema sõbrad</th>
                     <th class="text-center">Eemalda sõber</th>
                  </tr>
               </thead>
               <tbody>
                  <?php foreach($data as $person) : ?>
                  <tr class="success">
                     <td value=<?php echo $person['Kasutajanimi']?>> <?php echo $person['Kasutajanimi']?> </td>
                     <td> <?php echo $person['Sugu']?>, <?php echo $person['Vanus']?>, <?php echo $person['Asukoht']?> </td>
                     <?php $friends = showFriends($person['id'])?>
                     <td> <?php foreach($friends as $friend) : ?>
                        <?php echo $friend['Kasutajanimi']?> <?php endforeach ?> 
                     </td>
                     <td><a href="removeFriend.php?id=<?php echo $person['id']?>"
                        class="btn btn-danger" id="msg"><span
                        class="glyphicon glyphicon-remove"></span> Eemalda </a></td>
                  </tr>
                  <?php endforeach ?>
               </tbody>
            </table>
         </div>
         <div class="col-sm-6" style="overflow: auto">
            <h2>Sõbrakutsed</h2>
            <table class="table table-nonfluid table-bordered table-hover ">
               <?php $data = getFriends()?>
               <thead>
                  <tr class="active">
                     <th class="text-center">Kutse kellelt</th>
                     <th class="text-center">Võta kutse vastu</th>
                     <th class="text-center">Eira kutset</th>
                  </tr>
               </thead>
               <tbody>
                  <?php foreach($data as $person) : ?>
                  <form class="form-horizontal"
                     action="changeStatusConfirm.php" method="POST">
                     <tr class="success">
                        <td><?php echo $person['Kasutajanimi'];?></td>
                        <td><button type="submit" class="btn btn-success"
                           value=<?php echo $person['id']?> name="confirm" id="vasta">
                           <span class="glyphicon glyphicon-ok"></span> Võta vastu kutse
                           </button>
                        </td>
                  </form>
                  <form class="form-horizontal" action="changeStatusDeny.php"
                     method="POST">
                  <td><button type="submit" class="btn btn-danger"
                     value=<?php echo $person['id']?> name="deny" id="eira">
                  <span class="glyphicon glyphicon-remove"></span> Eira kutset
                  </button></td>
                  </form>
                  </tr>
                  <?php endforeach ?>
               </tbody>
            </table>
         </div>
      </div>
      <?php else : ?>
      <?php header("Location: logimine.php"); ?>
      <?php endif ?>
   </body>
</html>