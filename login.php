<?php
require("database.php");
$username = $_POST['Kasutajanimi'];
$password = $_POST['Parool'];
$success  = login($username, $password);
if ($success !== true) {
    header("Location: logimine.php?alert=" . $success);
} else {
    header("Location: login_success.php");
}
?>
